﻿namespace iTextPDFResize
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBoyutlandir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numOran = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numOran)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBoyutlandir
            // 
            this.btnBoyutlandir.Location = new System.Drawing.Point(163, 18);
            this.btnBoyutlandir.Name = "btnBoyutlandir";
            this.btnBoyutlandir.Size = new System.Drawing.Size(90, 29);
            this.btnBoyutlandir.TabIndex = 0;
            this.btnBoyutlandir.Text = "Boyutlandır";
            this.btnBoyutlandir.UseVisualStyleBackColor = true;
            this.btnBoyutlandir.Click += new System.EventHandler(this.btnBoyutlandir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Oran";
            // 
            // numOran
            // 
            this.numOran.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numOran.Location = new System.Drawing.Point(71, 24);
            this.numOran.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numOran.Name = "numOran";
            this.numOran.Size = new System.Drawing.Size(67, 20);
            this.numOran.TabIndex = 3;
            this.numOran.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 211);
            this.Controls.Add(this.numOran);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBoyutlandir);
            this.Name = "Form1";
            this.Text = "PDF Resize";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numOran)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBoyutlandir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numOran;
    }
}

