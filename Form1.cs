﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;

namespace iTextPDFResize
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBoyutlandir_Click(object sender, EventArgs e)
        {
            boyutlandir();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
         private void boyutlandir(){
        
PdfReader reader = new PdfReader(Application.StartupPath + @"\In.PDF");
Document doc = new Document(PageSize.A4, 0, 0, 0, 0);
PdfWriter writer = PdfWriter.GetInstance(doc,new FileStream(Application.StartupPath + @"\Out.PDF", FileMode.Create));
doc.Open();
PdfContentByte cb = writer.DirectContent;
float Scale = (float) numOran.Value/100;
var document = new Document(PageSize.A4);
MessageBox.Show("Sayfa" + reader.NumberOfPages.ToString());

for (var pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
{
PdfImportedPage page = writer.GetImportedPage(reader, pageNumber); //Tüm sayfaların kontrolü

/**Sayfayı ortala**/
var offsetX = (document.PageSize.Width - (page.Width * Scale)) / 2;
var offsetY = (document.PageSize.Height - (page.Height * Scale)) / 2;
/**Sayfayı ortala**/
cb.AddTemplate(page, Scale, 0, 0, Scale, offsetX, offsetY);
    /*Yeni Sayfa için yer aç :) */
doc.NewPage();
}



doc.Close();
         
         
         }
    }
}
